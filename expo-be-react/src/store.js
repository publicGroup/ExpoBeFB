/**
 * This file will allow access to the Redux's Store.
 * From Components, you can access it using 'connect', but for non-components code, there is no
 * simpler way.
 *
 * https://stackoverflow.com/a/41172768/2084731
 */

import { createStore, compose } from 'redux';
// import logger from 'redux-logger';
// import thunk from 'redux-thunk';
import { reactReduxFirebase } from 'react-redux-firebase';
import firebase from 'firebase';
// import 'firebase/firestore' // <- needed if using firestore
import rootReducer from './reducers';


const firebaseConfig = {
  apiKey: 'AIzaSyDGCJIRcYsLbHU8ko4ZnqRRWdrVp8NuFH4',
  authDomain: 'expobereact.firebaseapp.com',
  databaseURL: 'https://expobereact.firebaseio.com',
  projectId: 'expobereact',
  storageBucket: 'expobereact.appspot.com',
  messagingSenderId: '557379346073',
};

// react-redux-firebase options
const rrfConfig = {
  userProfile: 'users', // firebase root where user profiles are stored
  enableLogging: false, // enable/disable Firebase's database logging
  attachAuthIsReady: true, // attaches auth is ready promise to store
};

const initialState = {};
firebase.initializeApp(firebaseConfig);
// firebase.firestore() // <- needed if using firestore

const store = createStore(
  rootReducer,
  initialState,
  compose(reactReduxFirebase(firebase, rrfConfig)),
  // reduxFirestore(firebase) // <- needed if using firestore
  // applyMiddleware(thunk),
  // applyMiddleware(logger),
  // ),
);
export default store;
