import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, Switch } from 'react-router-dom';
import { Provider } from 'react-redux';

// Styles
import 'font-awesome/css/font-awesome.min.css';
import 'simple-line-icons/css/simple-line-icons.css';
import './scss/style.css';
import './scss/core/_dropdown-menu-right.scss';

import SignIn from './views/Pages/SignIn';
import SignUp from './views/Pages/SignUp';

import Full from './containers/Full/';
import ShowMessage from './components/ShowMessage/ShowMessage';

import store from './store';
import browserHistory from './history';


ReactDOM.render(
  (
    <Provider store={store}>
      <div>
        <ShowMessage />
        <Router history={browserHistory}>
          <Switch>
            <Route path="/signin" component={SignIn} />
            <Route path="/signup" component={SignUp} />
            {/* <Route path="/resetPassword" component={ResetPassword} /> */}

            <Route path="/" name="Home" component={Full} />
          </Switch>
        </Router>
      </div>
    </Provider>
  ), document.getElementById('root'),
);

