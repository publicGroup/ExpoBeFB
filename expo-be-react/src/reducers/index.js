import { combineReducers } from 'redux';
import { firebaseStateReducer } from 'react-redux-firebase';

import showMsgReducer from '../components/ShowMessage/reducers';

const rootReducer = combineReducers({
  showMsg: showMsgReducer,
  firebase: firebaseStateReducer,
  // firestore: firestoreReducer // <- needed if using firestore
});

export default rootReducer;
