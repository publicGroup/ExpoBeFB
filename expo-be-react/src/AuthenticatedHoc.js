/**
 * This is HighOrderComponent adds user profile data to other components
 *
 * It checks the user ia authenticated and authorized to access the given component, before forwarding it:
 * - If the login/logout is in progress, the Loading component is rendered
 * - If the user is not Authenticated (didn't sign in yet), the UnauthorizedPage component is rendered
 * - If the user is Authenticated but has no enough permissions/roles, the ForbiddenPage component is rendered
 * - Else (the user has signed in and has the required roles), the ComposedComponent is rendered.
 *
 * Params: ComposedComponent, requiredRoles = []
 * - ComposedComponent: The component to render if the user is allowed to access it
 * - requiredRoles: An array of roles the user must have to be allowed to access the ComposedComponent

 *
 * Usage, from ReactRouter:
 *  Only users with 'ADMIN' role:
 *    <Route path="admin/users" component={AuthenticatedHoc(ListUsers, [ROLES.ADMIN])} />
 *
 *  Any logged in user
 *    <Route path="registered/registeredUserPage" component={AuthenticatedHoc(RegisteredUserPage)} />
 *
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { firebaseConnect, isLoaded, isEmpty } from 'react-redux-firebase';
import { compose } from 'redux';
import _ from 'underscore';

import UnauthorizedPage from './views/Pages/Unauthorized';
import ForbiddenPage from './views/Pages/Forbidden';
import Loading from './components/Loading/Loading';

export default function (ComposedComponent, requiredRoles = []) {
  class AuthenticatedHoc extends Component {
    /**
     * Checks whether the user has the required roles
     */
    rolesOk() {
      const myRoles = this.props.profile.roles;
      const res = _.intersection(myRoles, requiredRoles).length > 0;
      // console.log('Check roles: ', res)
      return res;
    }

    render() {
      if (!isLoaded(this.props.auth) || !isLoaded(this.props.profile)) {
        return <Loading />;
      }
      if (isEmpty(this.props.auth)) {
        return <UnauthorizedPage />;
      }
      if (!this.rolesOk()) {
        return <ForbiddenPage requiredRoles={requiredRoles} />;
      }
      return <ComposedComponent {...this.props} />;
    }
  }

  function getDisplayName(WrappedComponent) {
    return WrappedComponent.displayName || WrappedComponent.name || 'Component';
  }

  AuthenticatedHoc.displayName = `AuthenticatedHoc(${getDisplayName(ComposedComponent)})`;

  AuthenticatedHoc.propTypes = {
    auth: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
    profile: PropTypes.shape({
      roles: PropTypes.arrayOf(PropTypes.string),
    }).isRequired,

  };

  return compose(
    firebaseConnect([
      {
        path: 'roles',
        storeAs: 'roles',
        // queryParams: ['orderByChild=createdBy', 'equalTo=123someuid'],
      }]),
    connect((state) => {
      const { data, auth, profile } = state.firebase;
      return { auth, profile, roles: data.roles };
    }),
  )(AuthenticatedHoc);
}
