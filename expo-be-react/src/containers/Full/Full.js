import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import { Container } from 'reactstrap';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { firebaseConnect } from 'react-redux-firebase';
// import PropTypes from 'prop-types';

import Header from '../../components/Header/Header';
import Sidebar from '../../components/Sidebar/';
import Breadcrumb from '../../components/Breadcrumb/';
import Aside from '../../components/Aside/';
import Footer from '../../components/Footer/';

import Home from '../../views/Pages/Home';

import AuthenticatedHoc from '../../AuthenticatedHoc';

import ListUsers from '../../views/Pages/Admin/ListUsers';
import CreateUser from '../../views/Pages/Admin/CreateUser';

import ListRooms from '../../views/Pages/Rooms/Rooms';
import CreateRoom from '../../views/Pages/Rooms/CreateRoom';
import ListTags from '../../views/Pages/Tags/Tags';
import CreateTag from '../../views/Pages/Tags/CreateTag';
import ListSpeakers from '../../views/Pages/Speakers/Speakers';
import CreateSpeaker from '../../views/Pages/Speakers/CreateSpeaker';

const ROLES = {
  PAID_USER: 'paidUser',
  ADMIN: 'admin',
};
const AuthedUser = (/* props */) => (
  <Switch>
    <Route path="/home" name="Home" component={Home} />


    {/* <Route path="/tags/new" component={AuthenticatedHoc(CreateTag, [ROLES.PAID_USER, ROLES.ADMIN])} /> */}
    {/* <Route path="/tags" component={AuthenticatedHoc(ListTags, [ROLES.PAID_USER, ROLES.ADMIN])} /> */}
    <Route path="/tags/new" component={CreateTag} />
    <Route path="/tags" component={ListTags} />

    <Route path="/rooms/new" component={AuthenticatedHoc(CreateRoom, [ROLES.PAID_USER, ROLES.ADMIN])} />
    <Route path="/rooms" component={AuthenticatedHoc(ListRooms, [ROLES.PAID_USER, ROLES.ADMIN])} />

    <Route
      path="/speakers/new"
      component={AuthenticatedHoc(CreateSpeaker, [ROLES.PAID_USER, ROLES.ADMIN])}
    />
    <Route path="/speakers" component={AuthenticatedHoc(ListSpeakers, [ROLES.PAID_USER, ROLES.ADMIN])} />

    <Route path="/admin/users/new" component={AuthenticatedHoc(CreateUser, [ROLES.ADMIN])} />
    <Route path="/admin/users" component={AuthenticatedHoc(ListUsers, [ROLES.PAID_USER, ROLES.ADMIN])} />

    <Redirect from="/" to="/home" />
  </Switch>
);

// const AnonUser = (props) => {
//   console.log('AnonUser')
//   return (
//     <Switch>
//       <Redirect from="/" to="/signin"/>
//     </Switch>
//   )
// }

const Full = props => (
  <div className="app">
    <Header />
    <div className="app-body">
      <Sidebar {...props} />
      <main className="main">
        <Breadcrumb />
        <Container fluid id="pageContainer">
          {AuthedUser(props)}
        </Container>
      </main>
      <Aside />
    </div>
    <Footer />
  </div>
);

// Full.propTypes = {
//   props: PropTypes.object.isRequired,
// };


export default compose(
  firebaseConnect(), // withFirebase can also be used
  connect(({ firebase: { auth } }) => ({ auth })),
)(Full);

