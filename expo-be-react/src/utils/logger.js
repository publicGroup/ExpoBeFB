// https://github.com/winstonjs/winston
// This is currently using for test/development. However we may want to log to some remote file or db:
// https://github.com/miguelmota/winston-remote
// https://github.com/winstonjs/winston-mongodb
// https://github.com/hisabimbola/winston-firebase
// note that if we do that, as this is the client, anyone might be able to log (unless firebase + logged in user)
const winston = require('winston');

const customColors = {
  trace: 'white',
  debug: 'blue',
  info: 'green',
  warn: 'yellow',
  crit: 'red',
  error: 'red',
};

const config = {
  colors: customColors,

  levels: {
    trace: 0,
    debug: 1,
    info: 2,
    warn: 3,
    crit: 4,
    error: 5,
  },
  transports: [
  ],
};

config.transports.push(new (winston.transports.Console)({
  name: 'consoleLogger',
  level: 'error',
  colorize: true,
  timestamp: true,
}));

const logger = new (winston.Logger)(config);
winston.addColors(customColors);

export default logger;
