import { Component } from 'react';
import $ from 'jquery';
import _ from 'underscore';

const validate = require('validate.js');


/**
 * A React.Component based abstract class, that facilitates checking form fields before submision.
 * This class ads a 'err' field in this.state, please do not modify it.
 * You can use:
 *  Usage:
 *  1- On your constructor, call super(props, initialState); And do not call this.state={...} in your constructor
 *  2- On your componentDidMount,call this.addValidator(...) once for each field
 *  3- On your render,  show the errors using {this.state.err[fieldId]}
 *
 * Example:
 * class YourComponent extends ValidableComponent{
 *   constructor(props) {
 *     super(props, initialState);
 *     ....
 *   }
 *
 *  componentDidMount() {
 *    this.addValidator('myInput', { presence: true, length: { minimum: 3 } });
 *  }
 *
 * render(){
 * ....
 *   <input id="myInput" type="text"/>
 *   <div className="myErrorClass">{this.state.err.myInput}</>
 * }
 *
 *
 *
 * About inheritance vs mixins, see discussion on
 * https://stackoverflow.com/questions/35301599/is-it-a-good-practice-to-create-base-
 * components-and-then-extend-them-in-react
 */
class ValidableComponent extends Component {
  /**
   *
   * @param props
   * @param state: instead of calling this.state={...},  pass the initial state to us
   */
  constructor(props, state) {
    super(props);
    this.privValidateInput = this.privValidateInput.bind(this);
    this.privOnBlurInput = this.privOnBlurInput.bind(this);
    this.state = Object.assign(state, { err: {} });
    this.fieldsToValidate = [];
  }

  privOnBlurInput(event) {
    const validatorData = event.data;
    const inputId = validatorData.inputId;
    const constrains = validatorData.constrains;
    this.privValidateInput(inputId, constrains);
  }

  privValidateInput(inputId, constrains) {
    const inputVal = this.state[inputId];
    const err = validate.single(inputVal, constrains);
    let errStr;
    if (err && err.length > 0) {
      errStr = err.join(',');
    }
    this.setState({
      err: Object.assign(this.state.err, { [inputId]: errStr }),
    });
    return errStr !== undefined;
  }

  /**
   * Rerun all validations.
   * It may seem enough to just check "_.any(this.state.err)" but for fields that are mandatory but that were not touch,
   * no 'blur' event was fired, hence no validation has run yet.
   */
  validateForm() {
    // To check if some field has errors we could just:
    // let errs = _.any(this.state.err);
    // However after resetErrs() or after component is mount, if we have some mandatory field, it will be empty
    // in such case, if the user doesn't change anything, no validation will be fired, hence the previous line will
    // result in errs=false;
    // To avoid that, we check every field. This will also show the specific error message on each field
    let errs = false;
    _.each(this.fieldsToValidate, (ftv) => {
      const inputId = ftv.inputId;
      const constrains = ftv.constrains;
      errs = this.privValidateInput(inputId, constrains) || errs;
    });
    return errs;
  }

  /**
   * You should call this, when user resets the form.
   * Note, this only reset the error messages, you should reset your fields' data
   */
  resetErrs() {
    this.setState({
      err: {},
    });
  }

  /**
   * Set given inputId constrains. For that input, onBlur will check the constrains are met.
   * After checking, this.state.err[inputId] will have an array of constrain violations (or will be undefined)
   * For example:
   *    <input id="myInput" type="text">...
   * @param inputId: The id of the input to check (myInput in the example)
   * @param constrains: The constrains as definied in https://validatejs.org/
   */
  addValidator(inputId, constrains) {
    const validatorData = { inputId, constrains };
    $(`#${inputId}`).on('blur', validatorData, this.privOnBlurInput);
    this.fieldsToValidate.push({ inputId, constrains });
  }
}

export { ValidableComponent };
