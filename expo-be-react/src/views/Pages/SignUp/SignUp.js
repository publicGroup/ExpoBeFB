import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { firebaseConnect } from 'react-redux-firebase';
import { withRouter } from 'react-router';
import { compose } from 'redux';

class SignUp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      error: {
        message: '',
      },
    };
  }

  signUp = () => {
    const { email, password } = this.state;
    this.setState({ error: { message: '' } });
    this.props.firebase.createUser({ email, password })
      .then((/* userData */) => {
        this.props.history.push('/');
      })
      .catch((error) => {
        this.setState({ error });
      });
  }

  render() {
    return (
      <div className="form-inline" style={{ margin: '5%' }}>
        <h2>Sign Up</h2>
        <div className="form-group">
          <input
            className="form-control"
            type="text"
            style={{ marginRight: '5px' }}
            placeholder="email"
            onChange={event => this.setState({ email: event.target.value })}
          />
          <input
            className="form-control"
            type="password"
            style={{ marginRight: '5px' }}
            placeholder="password"
            onChange={event => this.setState({ password: event.target.value })}
          />
          <button
            className="btn btn-primary"
            type="button"
            onClick={() => this.signUp()}
          >
            Sign Up
          </button>
        </div>
        <div>{this.state.error.message}</div>
        <div><a href="/signin">Already a user? Sign in instead</a></div>
      </div>
    );
  }
}


SignUp.propTypes = {
  firebase: PropTypes.shape({
    createUser: PropTypes.function,
  }).isRequired,
  history: PropTypes.shape({
    push: PropTypes.function,
  }).isRequired,
};


export default compose(firebaseConnect(), withRouter)(SignUp);
