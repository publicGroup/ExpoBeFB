import React from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { firebaseConnect } from 'react-redux-firebase';
import PropTypes from 'prop-types';
import ListItems from '../../../components/ListItems';

const COLLECTION = 'speakers';
const ITEMS = COLLECTION[0].toUpperCase() + COLLECTION.substr(1).toLowerCase();

const renderTableHeader = () => (
  <tr>
    <th>Name</th>
    <th>Last Name</th>
    <th>CV</th>
    <th>Country</th>
  </tr>
);

const render1item = (item, idx) => (
  <tr key={idx}>
    <td>{item.name}</td>
    <td>{item.lastName}</td>
    <td>{item.cv}</td>
    <td>{item.country}</td>
  </tr>
);

const Speakers = props => (
  <ListItems render1item={render1item} renderTableHeader={renderTableHeader} itemsName={ITEMS} {...props} />
);

Speakers.propTypes = {
  items: PropTypes.objectOf(PropTypes.shape({
    name: PropTypes.string.isRequired,
    lastName: PropTypes.string.isRequired,
    cv: PropTypes.string,
    country: PropTypes.string,
  })),
};

Speakers.defaultProps = {
  items: undefined,
};


export default compose(
  firebaseConnect([{
    path: COLLECTION,
    storeAs: COLLECTION,
    // queryParams: ['orderByChild=createdBy', 'equalTo=123someuid'],
  }]),
  connect(({ firebase: { data } }) =>
    // console.log('data', data)
    ({ items: data[COLLECTION] })),
)(Speakers);
