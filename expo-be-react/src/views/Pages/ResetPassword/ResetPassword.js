import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Container, Row, Col, CardGroup, Card, CardBody, Button, Input, InputGroup, InputGroupAddon } from 'reactstrap';
import { firebaseConnect } from 'react-redux-firebase';
import { withRouter } from 'react-router';
import { compose } from 'redux';

class ResetPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      error: {
        message: '',
      },
    };
  }

  resetPassword= () => {
    const { email } = this.state;
    this.props.firebase.auth().sendPasswordResetEmail(email)
      .then(() => {
        this.setState({ error: { message: 'Email sent' } });
      })
      .catch((error) => {
        this.setState({ error });
      });
  }

  render() {
    return (
      <div className="app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="8">
              <CardGroup>
                <Card className="p-4">
                  <CardBody>
                    <h1>Reset password</h1>
                    {/* <p className="text-muted">Your email</p> */}
                    <InputGroup className="mb-3">
                      <InputGroupAddon><i className="icon-user" /></InputGroupAddon>
                      <Input
                        type="text"
                        placeholder="user@example.com"
                        onChange={event => this.setState({ email: event.target.value })}
                      />
                    </InputGroup>
                    <Row>
                      <Col xs="6">
                        <Button color="primary" onClick={this.resetPassword} className="px-4">Reset</Button>
                      </Col>
                      <Col xs="6" className="text-right">
                        <a href="/signin" color="link" className="px-0">Sign in instead</a>
                      </Col>
                    </Row>
                    <div>{this.state.error.message}</div>
                  </CardBody>
                </Card>
                <Card className="text-white bg-primary py-5 d-md-down-none" style={{ width: `${44}%` }}>
                  <CardBody className="text-center">
                    <div>
                      <h2>Sign up</h2>
                      <p>Need an account?</p>
                      <a href="/signup" className="mt-3 px-3 py-2 bg-white " active="true"> Register Now! </a>
                    </div>
                  </CardBody>
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

ResetPassword.propTypes = {
  firebase: PropTypes.shape({
    auth: PropTypes.function,
  }).isRequired,
};


export default compose(firebaseConnect(), withRouter)(ResetPassword);

