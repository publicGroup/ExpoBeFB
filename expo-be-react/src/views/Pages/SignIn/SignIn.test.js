import React from 'react';
import ReactDOM from 'react-dom';
import { MemoryRouter } from 'react-router-dom';
import renderer from 'react-test-renderer';
import { Provider } from 'react-redux';
import store from '../../../store';

import SignIn from './SignIn';


// const store = createStore(reducers);

it('renders without crashing', () => {
  const div = document.createElement('div');
  const comp = (
    <Provider store={store}>
      <MemoryRouter>
        <SignIn />
      </MemoryRouter>
    </Provider>);
  ReactDOM.render(comp, div);
});


it('renders SignIn', () => {
  const comp = (
    <Provider store={store}>
      <MemoryRouter>
        <SignIn />
      </MemoryRouter>
    </Provider>);
  const component = renderer.create(comp);
  const tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});

