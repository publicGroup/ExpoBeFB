import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Container, Row, Col, CardGroup, Card, CardBody, Button, Input, InputGroup, InputGroupAddon } from 'reactstrap';
import { firebaseConnect } from 'react-redux-firebase';
import { withRouter } from 'react-router';
import { compose } from 'redux';

class SignIn extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      error: {
        message: '',
      },
    };
  }

  signIn= () => {
    this.setState({ error: { message: '' } });
    const { email, password } = this.state;
    this.props.firebase.login({ email, password })
      .then(() => {
        this.props.history.push('/');
      })
      .catch((error) => {
        this.setState({ error });
      });
  };

  render() {
    return (
      <div className="app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="8">
              <CardGroup>
                <Card className="p-4">
                  <CardBody>
                    <h1>Login</h1>
                    <p className="text-muted">Sign In to your account</p>
                    <InputGroup className="mb-3">
                      <InputGroupAddon><i className="icon-user" /></InputGroupAddon>
                      <Input
                        id="userEmail"
                        type="text"
                        placeholder="user@example.com"
                        onChange={event => this.setState({ email: event.target.value })}
                      />
                    </InputGroup>
                    <InputGroup className="mb-4">
                      <InputGroupAddon><i className="icon-lock" /></InputGroupAddon>
                      <Input
                        id="userPassword"
                        type="password"
                        placeholder="Password"
                        onChange={event => this.setState({ password: event.target.value })}
                      />
                    </InputGroup>
                    <Row>
                      <Col xs="6">
                        <Button id="loginButton" color="primary" onClick={this.signIn} className="px-4">Login</Button>
                      </Col>
                      <Col xs="6" className="text-right">
                        <a href="/resetPassword" color="link" className="px-0">Forgot password?</a>
                      </Col>
                    </Row>
                    <div>{this.state.error.message}</div>
                  </CardBody>
                </Card>
                <Card className="text-white bg-primary py-5 d-md-down-none" style={{ width: `${44}%` }}>
                  <CardBody className="text-center">
                    <div>
                      <h2>Sign up</h2>
                      <p>Need an account?</p>
                      <a href="/signup" className="mt-3 px-3 py-2 bg-white " active="true"> Register Now! </a>
                    </div>
                  </CardBody>
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

SignIn.propTypes = {
  firebase: PropTypes.shape({
    login: PropTypes.function,
  }).isRequired,
  history: PropTypes.shape({
    push: PropTypes.function,
  }).isRequired,
};


export default compose(firebaseConnect(), withRouter)(SignIn);

