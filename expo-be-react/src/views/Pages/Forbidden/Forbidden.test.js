import React from 'react';
import ReactDOM from 'react-dom';
import renderer from 'react-test-renderer';
import ForbiddenPage from './Forbidden';


it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<ForbiddenPage requiredRoles={['Role1', 'Role2']} />, div);
});


it('renders ForbiddenPage with roles', () => {
  const comp = (<ForbiddenPage requiredRoles={['Role1', 'Role2']} />);
  const component = renderer.create(comp);
  const tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});

it('renders ForbiddenPage with empty roles', () => {
  const comp = (<ForbiddenPage requiredRoles={[]} />);
  const component = renderer.create(comp);
  const tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});

it('renders ForbiddenPage without roles', () => {
  const comp = (<ForbiddenPage />);
  const component = renderer.create(comp);
  const tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
