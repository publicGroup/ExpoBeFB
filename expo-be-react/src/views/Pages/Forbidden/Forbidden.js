import React from 'react';
import PropTypes from 'prop-types';


function ForbiddenPage({ requiredRoles }) {
  return (
    <div>
      <div> You do not have permissions to visit this page.</div>
      <div>You should hold one of these roles: {requiredRoles.join(' ')}</div>
    </div>
  );
}

ForbiddenPage.propTypes = {
  requiredRoles: PropTypes.arrayOf(PropTypes.string),
};

ForbiddenPage.defaultProps = {
  requiredRoles: [],
};
export default ForbiddenPage;

