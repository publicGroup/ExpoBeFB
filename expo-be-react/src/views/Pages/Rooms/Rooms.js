import React from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { firebaseConnect } from 'react-redux-firebase';
import PropTypes from 'prop-types';
import ListItems from '../../../components/ListItems';

const COLLECTION = 'rooms';
const ITEMS = COLLECTION[0].toUpperCase() + COLLECTION.substr(1).toLowerCase();

const renderTableHeader = () => (
  <tr>
    <th>Name</th>
    <th>Direction</th>
    <th>Order</th>
  </tr>
);
const render1item = (item, idx) => (
  <tr key={idx}>
    <td>{item.name}</td>
    <td>{item.direction}</td>
    <td>{item.order}</td>
  </tr>
);
const Rooms = props => (
  <ListItems render1item={render1item} renderTableHeader={renderTableHeader} itemsName={ITEMS} {...props} />
);

Rooms.propTypes = {
  items: PropTypes.objectOf(PropTypes.shape({
    name: PropTypes.string.isRequired,
    direction: PropTypes.string,
    order: PropTypes.number.isRequired,
  })),
};

Rooms.defaultProps = {
  items: undefined,
};

export default compose(
  firebaseConnect([{
    path: COLLECTION,
    storeAs: COLLECTION,
    // queryParams: ['orderByChild=createdBy', 'equalTo=123someuid'],
  }]),
  connect(({ firebase: { data } }) =>
    // console.log('data', data)
    ({ items: data[COLLECTION] })),
)(Rooms);
