import React from 'react';
import { Row, Col, Button, Card, CardHeader, CardBody, Form, FormGroup, Label, Input } from 'reactstrap';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { firebaseConnect } from 'react-redux-firebase';
import _ from 'underscore';
import PropTypes from 'prop-types';

import { ValidableComponent } from '../../../utils/validator';
import { showMessage } from '../../../components/ShowMessage/actions';
import { MSG_TYPES } from '../../../components/ShowMessage/constants';

const COLLECTION = 'rooms';
const ITEM = 'Room';

const initialState = {
  name: '',
  order: 1,
  direction: '',
};

class CreateRoom extends ValidableComponent {
  constructor(props) {
    super(props, initialState);
  }

  componentDidMount() {
    this.addValidator('name', { presence: true, length: { minimum: 3 } });
    // this.addValidator('direction', {  length: { minimum: 3 } });
    this.addValidator('order', { presence: true, numericality: { onlyInteger: true, strict: true, greaterThan: 0 } });
  }

  handleChange= (event) => {
    event.preventDefault();
    const target = event.target;
    let value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.id;
    if (name === 'order') {
      value |= 0;// eslint-disable-line no-bitwise
    }
    this.setState({
      [name]: value,
    });
  }

  addItem= () => {
    if (this.validateForm()) {
      this.props.dispatch(showMessage(`${ITEM} not valid`, MSG_TYPES.ERROR));
      return;
    }
    // in theory this.state.name can change as firebase.push is async
    // so storing that to 'itemName' before calling firebase.push
    const itemName = this.state.name;
    this.props.firebase.push(COLLECTION, _.omit(this.state, 'err'))
      .then(() => {
        this.props.dispatch(showMessage(`${ITEM} ${itemName} added`, MSG_TYPES.SUCCESS));
        this.reset();
      }).catch((/* err */) => {
      // TODO: Log 'err' to firebase
      // https://gitlab.com/NovilGroup/ExpoBeFB/issues/8
        this.props.dispatch(showMessage('Can\'t add item ', MSG_TYPES.ERROR));
      });
  }

  reset= () => {
    this.setState(Object.assign({}, initialState));
    this.resetErrs();
  }


  render() {
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="12">
            <Card>
              <CardHeader>
                <strong>Create {ITEM}</strong>
                <small> Form</small>
              </CardHeader>
              <CardBody>
                <Form className="form-horizontal">

                  <Row>
                    <Col xs="12">
                      <FormGroup>
                        <Label htmlFor="name">Name</Label>
                        <Input
                          type="text"
                          id="name"
                          placeholder="Enter room name"
                          value={this.state.name}
                          onChange={this.handleChange}
                          {... _.isEmpty(this.state.name) ? {} : { valid: !this.state.err.name }}
                        />
                        <div className="input-error">{this.state.err.name}</div>
                      </FormGroup>
                    </Col>
                  </Row>

                  <Row>
                    <Col xs="12">
                      <FormGroup>
                        <Label htmlFor="name">Direction</Label>
                        <Input
                          type="text"
                          id="direction"
                          placeholder="Building A, 2nd floor"
                          value={this.state.direction}
                          onChange={this.handleChange}
                          {... _.isEmpty(this.state.direction) ? {} : { valid: !this.state.err.direction }}
                        />
                        <div className="input-error">{this.state.err.direction}</div>
                      </FormGroup>
                    </Col>
                  </Row>

                  <Row>
                    <Col xs="4">
                      <FormGroup>
                        <Label htmlFor="ccyear">Order
                          <small>(used in the grid)</small>
                        </Label>
                        <Input
                          type="select"
                          name="order"
                          id="order"
                          value={this.state.order}
                          onChange={this.handleChange}
                          {... _.isEmpty(this.state.order) ? {} : { valid: !this.state.err.order }}
                        >
                          {/* TODO: Get max room dynamically  https://gitlab.com/NovilGroup/ExpoBeFB/issues/6 */}
                          <option>1</option>
                          <option>2</option>
                          <option>3</option>
                          <option>4</option>
                          <option>5</option>
                          <option>6</option>
                          <option>7</option>
                          <option>8</option>
                          <option>9</option>
                          <option>10</option>
                        </Input>
                        <div className="input-error">{this.state.err.order}</div>
                      </FormGroup>
                    </Col>
                  </Row>

                  <div className="form-actions">
                    <Button id="saveBtn" onClick={this.addItem} color="primary">Save changes</Button>
                    <Button onClick={this.reset} color="secondary">Reset</Button>
                  </div>

                </Form>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}


CreateRoom.propTypes = {
  dispatch: PropTypes.func.isRequired,
};


export default compose(
  firebaseConnect([COLLECTION]),
  connect(({ firebase: { data } }) => ({ items: data[COLLECTION] })),
)(CreateRoom);
