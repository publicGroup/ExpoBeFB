import React from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { firebaseConnect } from 'react-redux-firebase';
import PropTypes from 'prop-types';
import ListItems from '../../../components/ListItems';

const COLLECTION = 'users';
const ITEMS = COLLECTION[0].toUpperCase() + COLLECTION.substr(1).toLowerCase();

const renderTableHeader = () => (
  <tr>
    <th>#</th>
    <th>NAME Name</th>
    <th>ROLE</th>
  </tr>
);

const render1item = (item, idx) => (
  <tr key={idx}>
    <td>{idx}</td>
    <td>{item.email}</td>
    <td>{item.roles ? Object.values(item.roles).join(', ') : '-'}</td>
  </tr>
);

const ListUsers = props => (
  <ListItems render1item={render1item} renderTableHeader={renderTableHeader} itemsName={ITEMS} {...props} />
);

ListUsers.propTypes = {
  items: PropTypes.objectOf(PropTypes.shape({
    email: PropTypes.string.isRequired,
    roles: PropTypes.arrayOf(PropTypes.string),
  })),
};

ListUsers.defaultProps = {
  items: undefined,
};

export default compose(
  firebaseConnect([{
    path: COLLECTION,
    storeAs: COLLECTION,
    // queryParams: ['orderByChild=createdBy', 'equalTo=123someuid'],
  }]),
  connect(({ firebase: { data } }) =>
    // console.log('data', data)
    ({ items: data[COLLECTION] })),
)(ListUsers);
