import React from 'react';

function UnauthorizedPage() {
  return (
    <div>
      <div> You must be logged in to access this page.</div>
      <div>Please  <a href="/signin">Sign in</a></div>
    </div>
  );
}

export default UnauthorizedPage;

