import React from 'react';
import ReactDOM from 'react-dom';
import renderer from 'react-test-renderer';
import { MemoryRouter } from 'react-router-dom';
import UnauthorizedPage from './Unauthorized';

it('renders without crashing', () => {
  const div = document.createElement('div');
  const comp = (
    <MemoryRouter>
      <UnauthorizedPage />
    </MemoryRouter>
  );
  ReactDOM.render(comp, div);
});


it('renders UnauthorizedPage', () => {
  const comp = (
    <MemoryRouter>
      <UnauthorizedPage />
    </MemoryRouter>
  );
  const component = renderer.create(comp);
  const tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});

