import React from 'react';
import { Row, Col, Button, Card, CardHeader, CardBody, Form, FormGroup, Label, Input } from 'reactstrap';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { firebaseConnect } from 'react-redux-firebase';
import _ from 'underscore';
import PropTypes from 'prop-types';

import { ValidableComponent } from '../../../utils/validator';
import { showMessage } from '../../../components/ShowMessage/actions';
import { MSG_TYPES } from '../../../components/ShowMessage/constants';

const COLLECTION = 'tags';
const ITEM = 'Tag';

const initialState = {
  name: '',
  color: '',
};

class CreateTag extends ValidableComponent {
  constructor(props) {
    super(props, initialState);
  }

  componentDidMount() {
    this.addValidator('name', { presence: true, length: { minimum: 3 } });
    const colorPattern = /#[a-fA-F0-9]{6}/;
    this.addValidator('color', {
      presence: true,
      format: {
        pattern: colorPattern,
        message: 'Should be and html-color-code: #AAFF33',
      },
    });
  }

  handleChange = (event) => {
    event.preventDefault();
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.id;
    this.setState({
      [name]: value,
    });
  }

  addItem = () => {
    if (this.validateForm()) {
      this.props.dispatch(showMessage(`${ITEM} not valid`, MSG_TYPES.ERROR));
      return;
    }
    // in theory this.state.name can change as firebase.push is async
    // so storing that to 'itemName' before calling firebase.push
    const itemName = this.state.name;
    this.props.firebase.push(COLLECTION, _.omit(this.state, 'err'))
      .then(() => {
        this.props.dispatch(showMessage(`${ITEM} ${itemName} added`, MSG_TYPES.SUCCESS));
        this.reset();
      }).catch((/* err */) => {
      // TODO: Log 'err' to firebase
      // https://gitlab.com/NovilGroup/ExpoBeFB/issues/8
        this.props.dispatch(showMessage('Can\'t add item ', MSG_TYPES.ERROR));
      });
  }

  reset = () => {
    this.setState(Object.assign({}, initialState));
    this.resetErrs();
  }

  render() {
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="12">
            <Card>
              <CardHeader>
                <strong>Create {ITEM}</strong>
                <small> Form</small>
              </CardHeader>
              <CardBody>
                <Form className="form-horizontal">

                  <Row>
                    <Col xs="12">
                      <FormGroup>
                        <Label htmlFor="name">Name</Label>
                        <Input
                          type="text"
                          id="name"
                          placeholder="Enter tag name"
                          value={this.state.name}
                          onChange={this.handleChange}
                          {... _.isEmpty(this.state.name) ? {} : { valid: !this.state.err.name }}
                        />
                        <div className="input-error">{this.state.err.name}</div>
                      </FormGroup>
                    </Col>
                  </Row>

                  <Row>
                    <Col xs="12">
                      <FormGroup>
                        <Label htmlFor="name">Tag color</Label>
                        {/* Todo: use a color picker https://gitlab.com/NovilGroup/ExpoBeFB/issues/7 */}
                        <Input
                          type="text"
                          id="color"
                          placeholder="#C2AB11"
                          value={this.state.color}
                          onChange={this.handleChange}
                          {... _.isEmpty(this.state.color) ? {} : { valid: !this.state.err.color }}
                        />
                        <div className="input-error">{this.state.err.color}</div>
                      </FormGroup>
                    </Col>
                  </Row>

                  <div className="form-actions">
                    <Button id="saveBtn" onClick={this.addItem} color="primary">Save changes</Button>
                    <Button onClick={this.reset} color="secondary">Reset</Button>
                  </div>
                </Form>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

CreateTag.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

export default compose(
  firebaseConnect([COLLECTION]),
  connect(({ firebase: { data } }) => ({ items: data[COLLECTION] })),
)(CreateTag);
