/**
 * This file will allow access to ReactRouter's History.
 * From Components, you can access it using 'withHistory', but for non-components code, there is no
 * simpler way.
 *
 * Another option will be to use react-router-redux or similar package, to be able to dispatch
 * history.push events from Redux reducers, but that package is in Beta yet, so using this option
 * meanwhile.
 *
 * https://stackoverflow.com/a/42679052/2084731
 */

import createBrowserHistory from 'history/createBrowserHistory';

export default createBrowserHistory();
