// This file, comes from http://coreui.io/ - https://github.com/mrholek/CoreUI-React
// https://github.com/mrholek/CoreUI-React/commit/338d579c021ec289da44d004bde4910bd5ec4d42
// - commit 338d579c021ec289da44d004bde4910bd5ec4d42 - v1.0.4 -
// https://github.com/mrholek/CoreUI-React/archive/v1.0.4.tar.gz
// To allow easier update (if necessary) make eslint ignore some rules
/* eslint class-methods-use-this: [0] */
/* eslint max-len :[0] */

import React, { Component } from 'react';

class SidebarMinimizer extends Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    this.sidebarMinimize();
    this.brandMinimize();
  }

  sidebarMinimize() {
    document.body.classList.toggle('sidebar-minimized');
  }

  brandMinimize() {
    document.body.classList.toggle('brand-minimized');
  }

  render() {
    return (
      <button className="sidebar-minimizer" type="button" onClick={this.handleClick} />
    );
  }
}

export default SidebarMinimizer;
