import React from 'react';
import renderer from 'react-test-renderer';
import { MemoryRouter } from 'react-router-dom';
import { createStore } from 'redux';
import { Provider } from 'react-redux';

import reducers from '../../reducers';
import SidebarMinimizer from './SidebarMinimizer';

const store = createStore(reducers);

it('renders SidebarHeader ', () => {
  const comp = (
    <Provider store={store}>
      <MemoryRouter initialEntries={['/']}>
        <SidebarMinimizer />
      </MemoryRouter>
    </Provider>
  );
  const component = renderer.create(comp);
  const tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});

