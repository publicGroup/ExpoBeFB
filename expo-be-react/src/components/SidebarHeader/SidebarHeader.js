// This file, comes from http://coreui.io/ - https://github.com/mrholek/CoreUI-React
// https://github.com/mrholek/CoreUI-React/commit/338d579c021ec289da44d004bde4910bd5ec4d42
// - commit 338d579c021ec289da44d004bde4910bd5ec4d42 - v1.0.4 -
import React, { Component } from 'react';

class SidebarHeader extends Component {
  render() {
    // return null
    // Uncomment following code lines to add Sidebar Header
    return (
      <div className="sidebar-header" />
    );
  }
}

export default SidebarHeader;
