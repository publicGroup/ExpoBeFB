export default {
  items: [
    // {
    //   name: 'Dashboard',
    //   url: '/dashboard',
    //   icon: 'icon-speedometer',
    //   badge: {
    //     variant: 'info',
    //     text: 'NEW',
    //   },
    // },
    {
      name: 'Admin',
      url: '/admin',
      icon: 'fa fa-unlock-alt',
      children: [
        {
          name: 'List Users',
          url: '/admin/users',
        },
        {
          name: 'Create User',
          url: '/admin/users/new',
        },
      ],
    },
    {
      name: 'Rooms',
      url: '/rooms',
      icon: 'fa fa-map-marker',
      children: [
        {
          name: 'List Rooms',
          url: '/rooms',
        },
        {
          name: 'Create Room',
          url: '/rooms/new',
        },
      ],
    },
    {
      name: 'Tags',
      url: '/tags',
      icon: 'icon-tag',
      children: [
        {
          name: 'List Tags',
          url: '/tags',
        },
        {
          name: 'Create Tag',
          url: '/tags/new',
        },
      ],
    },
    {
      name: 'Speakers',
      url: '/tags',
      icon: 'icon-people',
      children: [
        {
          name: 'List Speakers',
          url: '/speakers',
        },
        {
          name: 'Create Speaker',
          url: '/speakers/new',
        },
      ],
    },

  ],
};

