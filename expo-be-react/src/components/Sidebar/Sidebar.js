// This file, comes from http://coreui.io/ - https://github.com/mrholek/CoreUI-React
// https://github.com/mrholek/CoreUI-React/commit/338d579c021ec289da44d004bde4910bd5ec4d42
// - commit 338d579c021ec289da44d004bde4910bd5ec4d42 - v1.0.4 -
// https://github.com/mrholek/CoreUI-React/archive/v1.0.4.tar.gz
// To allow easier update (if necessary) make eslint ignore some rules
/* eslint class-methods-use-this: [0] */
/* eslint prefer-destructuring:[0] */
/* eslint jsx-a11y/anchor-is-valid :[0] */
/* eslint no-nested-ternary:[0] */
/* eslint max-len :[0] */
/* eslint no-use-before-define :[0] */


import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import { Badge, Nav, NavItem, NavLink as RsNavLink } from 'reactstrap';
import classNames from 'classnames';
import nav from './_nav';
import SidebarFooter from './../SidebarFooter';
import SidebarForm from './../SidebarForm';
import SidebarHeader from './../SidebarHeader';
import SidebarMinimizer from './../SidebarMinimizer';

class Sidebar extends Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(e) {
    e.preventDefault();
    e.target.parentElement.classList.toggle('open');
  }

  activeRoute(routeName, props) {
    // return this.props.location.pathname.indexOf(routeName) > -1
    // ? 'nav-item nav-dropdown open' : 'nav-item nav-dropdown';
    return props.location.pathname.indexOf(routeName) > -1 ? 'nav-item nav-dropdown open' : 'nav-item nav-dropdown';
  }

  // https://gitlab.com/NovilGroup/ExpoBeFB/issues/3
  // todo Sidebar nav secondLevel
  // secondLevelActive(routeName) {
  //   return this.props.location.pathname.indexOf(routeName) > -1
  // ? "nav nav-second-level collapse in" : "nav nav-second-level collapse";
  // }


  render() {
    const props = this.props;
    const activeRoute = this.activeRoute;

    // badge addon to NavItem
    const badge = (bdg) => {
      if (bdg) {
        const classes = classNames(bdg.class);
        return (<Badge className={classes} color={bdg.variant}>{bdg.text}</Badge>);
      }
      return (<div />);
    };

    // simple wrapper for nav-title item
    const wrapper = item => (item.wrapper && item.wrapper.element ? (React.createElement(item.wrapper.element, item.wrapper.attributes, item.name)) : item.name);

    // nav list section title
    const title = (ttl, key) => {
      const classes = classNames('nav-title', ttl.class);
      return (<li key={key} className={classes}>{wrapper(ttl)} </li>);
    };

    // nav list divider
    const divider = (dvdr, key) => (<li key={key} className="divider" />);

    // nav item with nav link
    const navItem = (item, key) => {
      const classes = classNames(item.class);
      const isExternal = url => (url.substring(0, 4) === 'http');
      const variant = classNames('nav-link', item.variant ? `nav-link-${item.variant}` : '');
      const itemId = item.name.replace(/ /g, '_');
      return (
        <NavItem key={key} className={classes}>
          {isExternal(item.url) ?
            <RsNavLink href={item.url} className={variant} active id={itemId}>
              <i className={item.icon} />{item.name}{badge(item.badge)}
            </RsNavLink>
            :
            <NavLink to={item.url} className={variant} activeClassName="active" id={itemId}>
              <i className={item.icon} />{item.name}{badge(item.badge)}
            </NavLink>
          }
        </NavItem>
      );
    };

    // nav dropdown
    const navDropdown = (item, key) => {
      const itemId = item.name.replace(/ /g, '_');
      return (
        <li key={key} className={activeRoute(item.url, props)} id={itemId}>
          <a className="nav-link nav-dropdown-toggle" href="#a" onClick={this.handleClick}><i
            className={item.icon}
          />{item.name}
          </a>
          <ul className="nav-dropdown-items">
            {navList(item.children)}
          </ul>
        </li>);
    };

    // nav link
    const navLink = (item, idx) =>
      (item.title ? title(item, idx) :
        item.divider ? divider(item, idx) :
          item.children ? navDropdown(item, idx)
            : navItem(item, idx));

    // nav list
    const navList = items => items.map((item, index) => navLink(item, index));

    // sidebar-nav root
    return (
      <div className="sidebar">
        <SidebarHeader />
        <SidebarForm />
        <nav className="sidebar-nav">
          <Nav>
            {navList(nav.items)}
          </Nav>
        </nav>
        <SidebarFooter />
        <SidebarMinimizer />
      </div>
    );
  }
}

export default Sidebar;
