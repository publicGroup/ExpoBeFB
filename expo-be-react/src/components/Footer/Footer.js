// This file, comes from http://coreui.io/ - https://github.com/mrholek/CoreUI-React
// https://github.com/mrholek/CoreUI-React/commit/338d579c021ec289da44d004bde4910bd5ec4d42
// - commit 338d579c021ec289da44d004bde4910bd5ec4d42 - v1.0.4 -
import React, { Component } from 'react';

class Footer extends Component {
  render() {
    return (
      <footer className="app-footer">
        <span><a href="http://novil.com.ar">Novil</a> &copy; 2017 Novil.</span>
      </footer>
    );
  }
}

export default Footer;
