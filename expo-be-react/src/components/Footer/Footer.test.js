import React from 'react';
import renderer from 'react-test-renderer';
import Footer from './Footer';


it('renders Footer ', () => {
  const comp = (<Footer />);
  const component = renderer.create(comp);
  const tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});

