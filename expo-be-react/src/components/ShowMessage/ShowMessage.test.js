import React from 'react';
import ReactDOM from 'react-dom';
import renderer from 'react-test-renderer';
import { Provider } from 'react-redux';
// import { createStore } from 'redux';
import store from '../../store';
import ShowMessage from './ShowMessage';
// import reducers from './reducers';

// const store = createStore(reducers);


it('renders without crashing', () => {
  const div = document.createElement('div');
  const comp = (
    <Provider store={store}>
      <ShowMessage />
    </Provider>
  );
  ReactDOM.render(comp, div);
});


it('renders ShowMessage', () => {
  const comp = (
    <Provider store={store}>
      <ShowMessage />
    </Provider>
  );
  const component = renderer.create(comp);
  const tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});

