import { SHOW_MESSAGE, MSG_TYPES } from './constants';

import * as actions from './actions';

const msgText = 'Some message text';

it('should create a SHOW_MESSAGE SUCCESS action  ', () => {
  const act = actions.showMessage(msgText, MSG_TYPES.SUCCESS);
  expect(act).toHaveProperty('type', SHOW_MESSAGE);
  expect(act.payload).toHaveProperty('text', msgText);
  expect(act.payload).toHaveProperty('type', MSG_TYPES.SUCCESS);
  expect(act.payload).toHaveProperty('timestamp');
});

it('should create a SHOW_MESSAGE ERROR action  ', () => {
  const act = actions.showMessage(msgText, MSG_TYPES.ERROR);
  expect(act).toHaveProperty('type', SHOW_MESSAGE);
  expect(act.payload).toHaveProperty('text', msgText);
  expect(act.payload).toHaveProperty('type', MSG_TYPES.ERROR);
  expect(act.payload).toHaveProperty('timestamp');
});

it('should create a SHOW_MESSAGE WARNING action  ', () => {
  const act = actions.showMessage(msgText, MSG_TYPES.WARNING);
  expect(act).toHaveProperty('type', SHOW_MESSAGE);
  expect(act.payload).toHaveProperty('text', msgText);
  expect(act.payload).toHaveProperty('type', MSG_TYPES.WARNING);
  expect(act.payload).toHaveProperty('timestamp');
});
