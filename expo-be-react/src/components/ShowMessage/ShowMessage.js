import React, { Component } from 'react';
import { connect } from 'react-redux';
import { ToastContainer, toast } from 'react-toastify';
import _ from 'underscore';
import PropTypes from 'prop-types';

import 'react-toastify/dist/ReactToastify.min.css';


import { MSG_TYPES } from './constants';

class ShowMessage extends Component {
  constructor(props = { text: null }) {
    super(props);
  }
  componentWillReceiveProps(nextProps) {
    if (_.isEqual(nextProps, this.props)) {
      return;
    }
    if (!nextProps.text || !nextProps.timestamp || !nextProps.type) {
      // TODO: Log in FB invalid message
      return;
    }
    toast[nextProps.type](nextProps.text);
  }

  render() {
    // console.log("rend");
    return (
      <ToastContainer
        position="top-right"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        pauseOnHover
        style={{ zIndex: '99999999' }}
      />
    );
  }
}

ShowMessage.propTypes = {
  text: PropTypes.string, // eslint-disable-line  react/require-default-props
  // we use timestamp in _.isEqual
  // eslint-disable-next-line react/no-unused-prop-types
  timestamp: PropTypes.number, // eslint-disable-line  react/require-default-props
  type: PropTypes.oneOf(_.values(MSG_TYPES)), // eslint-disable-line  react/require-default-props
};

export default connect(state => state.showMsg || { text: null })(ShowMessage);
