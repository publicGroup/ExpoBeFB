import { SHOW_MESSAGE } from './constants';


export default (state = {}, action) => {
  switch (action.type) {
  case
    SHOW_MESSAGE:
    return Object.assign({}, action.payload); // we overwritte any previous message, as it was alredy dispatched/shown

  default:
    return state;
  }
};

