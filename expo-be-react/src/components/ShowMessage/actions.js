import { SHOW_MESSAGE } from './constants';

export function showMessage(text, type) {
  const action = {
    type: SHOW_MESSAGE,
    payload: {
      text,
      type,
      timestamp: Date.now(),
    },
  };
  return action;
}

