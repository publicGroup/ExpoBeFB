export const SHOW_MESSAGE = 'SHOW_MESSAGE';

export const MSG_TYPES = {
  SUCCESS: 'success',
  ERROR: 'error',
  WARNING: 'warning',
};

