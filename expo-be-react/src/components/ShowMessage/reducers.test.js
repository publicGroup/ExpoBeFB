import { SHOW_MESSAGE, MSG_TYPES } from './constants';
import reducer from './reducers';

it('should return the initial state', () => {
  expect(reducer(undefined, {})).toEqual({});
});

it('should ignore unknown message', () => {
  expect(reducer(undefined, { type: 'SOME_UNEXISTING_TYPE' })).toEqual({});
  const prevState = [{ text: 'Old msg', type: MSG_TYPES.ERROR, timestamp: 2 }];
  expect(reducer(prevState, { type: 'SOME_UNEXISTING_TYPE' }))
    .toEqual(prevState);
});

it('should return the new message state', () => {
  const timestamp = Date.now();
  const showMsgAction = {
    type: SHOW_MESSAGE,
    payload: {
      text: 'Some text',
      type: MSG_TYPES.SUCCESS,
      timestamp,
    },
  };
  expect(reducer(undefined, showMsgAction)).toEqual(showMsgAction.payload);
});

it('should overwrite any old message', () => {
  const timestamp = Date.now();
  const showMsgAction = {
    type: SHOW_MESSAGE,
    payload: {
      text: 'Some text',
      type: MSG_TYPES.SUCCESS,
      timestamp,
    },
  };
  const prevState = { text: 'Old msg', type: MSG_TYPES.ERROR, timestamp };
  expect(reducer([prevState], showMsgAction)).toEqual(showMsgAction.payload);
});
