import React from 'react';
import { Row, Col, Card, CardHeader, CardBody, Table } from 'reactstrap';
import { isLoaded, isEmpty } from 'react-redux-firebase';
import _ from 'underscore';
import PropTypes from 'prop-types';
import Loading from '../Loading';

const ListItems = props => (
  <div className="animated fadeIn">
    <Row>
      <Col xs="12">
        <Card>
          <CardHeader>
            List {props.itemsName}
          </CardHeader>
          <CardBody>
            <Table responsive striped>
              <thead>
                {props.renderTableHeader()}
              </thead>
              <tbody>
                {
                  isLoaded(props.items) &&
                _.map(props.items, (item, idx) =>
                  props.render1item(item, idx))
                }
              </tbody>
            </Table>
            {!isLoaded(props.items) &&
            <Loading />
            }
            {
              isEmpty(props.items) &&
              <div className="emptyList"> No {props.itemsName} to show</div>
            }
          </CardBody>
        </Card>
      </Col>
    </Row>
  </div>
);

ListItems.propTypes = {
  items: PropTypes.object, // eslint-disable-line react/forbid-prop-types
  renderTableHeader: PropTypes.func.isRequired,
  render1item: PropTypes.func.isRequired,
  itemsName: PropTypes.string.isRequired,
};

ListItems.defaultProps = {
  items: undefined,
};

export default ListItems;
