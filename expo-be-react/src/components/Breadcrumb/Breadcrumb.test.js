import React from 'react';
import renderer from 'react-test-renderer';
import { MemoryRouter } from 'react-router-dom';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import Breadcrumb from './Breadcrumb';

import reducers from '../../reducers';

const store = createStore(reducers);

it('renders Breadcrumb ', () => {
  const comp = (
    <Provider store={store}>
      <MemoryRouter initialEntries={['/']}>
        <Breadcrumb location={{ pathname: '/' }} />
      </MemoryRouter>
    </Provider>
  );
  const component = renderer.create(comp);
  const tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});

