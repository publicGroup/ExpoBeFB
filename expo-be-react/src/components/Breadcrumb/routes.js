// TODO: this is used by Breadcrumb only. Move there.
const routes = {

  '/tags': 'Tags',
  '/tags/new': 'New',

  '/rooms': 'Rooms',
  '/rooms/new': 'New',

  '/speakers': 'Speakers',
  '/speakers/new': 'New',

  '/admin': 'Admin',
  '/admin/users': 'Users',
  '/admin/users/new': 'New',
};

export default routes;
