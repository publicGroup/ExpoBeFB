import React from 'react';
import renderer from 'react-test-renderer';
import { MemoryRouter } from 'react-router-dom';
import { createStore } from 'redux';
import { Provider } from 'react-redux';

import reducers from '../../reducers';
import Aside from './Aside';

const store = createStore(reducers);

it('renders Header ', () => {
  const comp = (
    <Provider store={store}>
      <MemoryRouter>
        <Aside />
      </MemoryRouter>
    </Provider>
  );
  const component = renderer.create(comp);
  const tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});

