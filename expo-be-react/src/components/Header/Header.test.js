import React from 'react';
import renderer from 'react-test-renderer';
import { MemoryRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import Header from './Header';
import store from '../../store';
// import reducers from '../../reducers';
//
// const store = createStore(reducers);

it('renders Header ', () => {
  const comp = (
    <Provider store={store}>
      <MemoryRouter>
        <Header />
      </MemoryRouter>
    </Provider>
  );
  const component = renderer.create(comp);
  const tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});

