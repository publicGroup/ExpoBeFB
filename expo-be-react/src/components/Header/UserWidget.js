import React, { Component } from 'react';
import { withRouter } from 'react-router';
import PropTypes from 'prop-types';
import { NavDropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { firebaseConnect, isLoaded, isEmpty } from 'react-redux-firebase';
import Loading from '../Loading';

class UserWidget extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dropdownOpen: false,
    };
  }

  toggle = () => {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen,
    });
  }

  signOut = () => {
    this.props.firebase.logout();
    this.props.history.push('/');
  }

  signIn = () => {
    this.props.history.push('/signin');
  }

  renderAnon() {
    return (
      <button className="btn btn-danger" onClick={this.signIn}>
        Sign In
      </button>
    );
  }

  renderAuth() {
    const email = this.props.auth.email;
    const name = email.substring(0, email.indexOf('@'));
    return (
      <NavDropdown isOpen={this.state.dropdownOpen} toggle={this.toggle}>
        <DropdownToggle nav>
          <span id="usernameWidget" className="fa fa-user-circle"> &nbsp; {name}</span>
        </DropdownToggle>
        <DropdownMenu>
          <DropdownItem onClick={this.signOut}> SignOut</DropdownItem>
        </DropdownMenu>
      </NavDropdown>
    );
  }

  render() {
    if (!isLoaded(this.props.auth)) {
      return (<Loading />);
    }
    if (isEmpty(this.props.auth)) {
      return this.renderAnon();
    }
    return this.renderAuth();
  }
}

UserWidget.propTypes = {
  history: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
  firebase: PropTypes.shape({
    logout: PropTypes.func.isRequired,
  }).isRequired,
  auth: PropTypes.shape({
    email: PropTypes.string,
  }).isRequired,

};

export default compose(
  firebaseConnect([
    'users', // { path: '/users' } // object notation
  ]),
  withRouter,
  connect(({ firebase: { auth, profile } }) => ({
    auth,
    profile,
  })),
)(UserWidget);
