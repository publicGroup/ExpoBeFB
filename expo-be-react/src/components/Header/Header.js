// This file, comes from http://coreui.io/ - https://github.com/mrholek/CoreUI-React
// https://github.com/mrholek/CoreUI-React/commit/338d579c021ec289da44d004bde4910bd5ec4d42
// - commit 338d579c021ec289da44d004bde4910bd5ec4d42 - v1.0.4 -
// https://github.com/mrholek/CoreUI-React/archive/v1.0.4.tar.gz
// To allow easier update (if necessary) make eslint ignore some rules
/* eslint class-methods-use-this: [0] */

import React, { Component } from 'react';
import {
  // Nav,
  // NavItem,
  NavbarToggler,
  NavbarBrand,
} from 'reactstrap';

import UserWidget from './UserWidget';

class Header extends Component {
  sidebarToggle(e) {
    e.preventDefault();
    document.body.classList.toggle('sidebar-hidden');
  }

  sidebarMinimize(e) {
    e.preventDefault();
    document.body.classList.toggle('sidebar-minimized');
  }

  mobileSidebarToggle(e) {
    e.preventDefault();
    document.body.classList.toggle('sidebar-mobile-show');
  }

  asideToggle(e) {
    e.preventDefault();
    document.body.classList.toggle('aside-menu-hidden');
  }

  render() {
    return (
      <header className="app-header navbar">
        <NavbarToggler className="d-lg-none" onClick={this.mobileSidebarToggle}>
          <span className="navbar-toggler-icon" />
        </NavbarToggler>
        <NavbarBrand href="#" />


        <NavbarToggler className="d-md-down-none mr-auto" onClick={this.sidebarToggle}>
          <span className="navbar-toggler-icon" />
        </NavbarToggler>
        <UserWidget />

        {/* <NavbarToggler className="d-md-down-none" onClick={this.asideToggle}> */}
        {/* <span className="navbar-toggler-icon" /> */}
        {/* </NavbarToggler> */}
      </header>
    );
  }
}

export default Header;
