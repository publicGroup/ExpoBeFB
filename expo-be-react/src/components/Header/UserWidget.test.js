import React from 'react';
import renderer from 'react-test-renderer';
import { MemoryRouter } from 'react-router-dom';
// import { createStore } from 'redux';
import { Provider } from 'react-redux';
import UserWidget from './UserWidget';
import store from '../../store';
// import reducers from '../../reducers';
//
// // const store = createStore(reducers);

it('renders UserWidget ', () => {
  const comp = (
    <Provider store={store}>
      <MemoryRouter>
        <UserWidget />
      </MemoryRouter>
    </Provider>
  );
  const component = renderer.create(comp);
  const tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});

