# Pushing to firebase
Change the project name (expobereact here) to whatever your project name is

## 1 Setup
- npm i -g firebase-tools
- firebase login:ci
- firebase init 
   select Realtime DB, and use fixtures/database.rules.json

## 2 Init DB:
- firebase --project expobereact auth:import fixtures/auth.json
- firebase --project expobereact database:set / fixtures/database.json

## 3 Push/deploy
- npm run build
- firebase deploy --project expobereact 


## Other usefull commands:
-  firebase serve # to run local
- firebase --project expobereact auth:export fixtures/auth.json
- firebase --project expobereact database:get / > fixtures/database.json

