/* global  cy */

describe('Login tests', () => {
  beforeEach(() => {
    // alias the users fixtures
    cy.fixture('users.json').as('users');
  });

  it('Valid user can login!', function validUserCanLogin() {
    const adminUser = this.users.adminUser;
    cy.login(adminUser.email, adminUser.password);
    cy.get('#usernameWidget').should('contain', adminUser.email.split('@')[0]);
    // wait until home page is loaded before returning
    cy.url().should('include', '/home');
    cy.get('#pageContainer').should('contain', 'This is the Home page');
  });

  it('Invalid pass fails!', function invalidPassFail() {
    const adminUser = this.users.adminUser;
    cy.login(adminUser.email, `${adminUser.password}sometingToFail`);
    cy.get('#usernameWidget').should('not.contain', adminUser.email.split('@')[0]);
    cy.url().should('include', '/signin');
    cy.get('#root').should('contain', 'The password is invalid or the user does not have a password.');
  });
});
