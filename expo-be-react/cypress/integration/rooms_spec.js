/* global  cy */

describe('Rooms tests', () => {
  beforeEach(() => {
    // alias the users fixtures
    cy.fixture('users.json').as('users');
  });

  it('Create rooms!', function createRooms() {
    const rnd = +Date.now();
    const adminUser = this.users.adminUser;
    const roomName = `room_${rnd}`;
    const roomDirection = `dir_${rnd}`;
    cy.login(adminUser.email, adminUser.password);
    cy.url().should('include', '/home');

    // go to Create Rooms page
    cy.get('#Rooms').click();
    cy.get('#Create_Room').click();
    cy.url().should('include', '/rooms/new');
    cy.get('#pageContainer').should('contain', 'Create Room'); // wait until its shown.
    // add the Room
    cy.get('#name').type(roomName).should('have.value', roomName);
    cy.get('#direction').type(roomDirection).should('have.value', roomDirection);
    cy.get('#saveBtn').click();
    // we should stay on same page after saving
    cy.url().should('include', '/rooms/new');
    cy.get('#pageContainer').should('contain', 'Create Room'); // wait until its shown.
    cy.get('#name').type(roomName).should('be.empty');
    cy.get('#direction').type(roomDirection).should('be.empty');
    // lets check it was created in Rooms List
    cy.get('#List_Rooms').click();
    cy.url().should('include', '/rooms');
    cy.get('#pageContainer').should('contain', 'List Rooms'); // wait until its shown.
    cy.get('#pageContainer').should('contain', roomName);
    cy.get('#pageContainer').should('contain', roomDirection);
  });
});

