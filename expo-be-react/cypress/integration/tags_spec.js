/* global  cy */

describe('Tags tests', () => {
  beforeEach(() => {
    // alias the users fixtures
    cy.fixture('users.json').as('users');
  });

  it('Create tags!', function createTags() {
    const adminUser = this.users.adminUser;
    const tagName = `tag_${Date.now()}`;
    const tagColor = '#ABCDEF';
    cy.login(adminUser.email, adminUser.password);
    cy.url().should('include', '/home');

    // go to Create Tags page
    cy.get('#Tags').click();
    cy.get('#Create_Tag').click();
    cy.url().should('include', '/tags/new');
    cy.get('#pageContainer').should('contain', 'Create Tag'); // wait until its shown.
    // add the tag
    cy.get('#name').type(tagName).should('have.value', tagName);
    cy.get('#color').type(tagColor).should('have.value', tagColor);
    cy.get('#saveBtn').click();
    // we should stay on same page after saving
    cy.url().should('include', '/tags/new');
    cy.get('#pageContainer').should('contain', 'Create Tag'); // wait until its shown.
    cy.get('#name').type(tagName).should('be.empty');
    cy.get('#color').type(tagColor).should('be.empty');
    // lets check it was created in Tags List
    cy.get('#List_Tags').click();
    cy.url().should('include', '/tags');
    cy.get('#pageContainer').should('contain', 'List Tags'); // wait until its shown.
    cy.get('#pageContainer').should('contain', tagName);
    cy.get('#pageContainer').should('contain', tagColor);
  });
});

