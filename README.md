# Expo Be FB
## A backoffice/admin for conferences' schedules

#### Stack
Firebase DB

Firebase Auth

React+Redux 

Reactstrap (Boostrap 4 for react)

Testing: Enzyme, Jest, Cypress


### Usage
git clone ...

cd expo-be-react

npm install

npm run build-css

npm run test

npm start

Check docs/ directory for more info

## A working version can be found on 
https://expobereact.firebaseapp.com/
